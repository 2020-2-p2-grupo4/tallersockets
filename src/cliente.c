#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h> 
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <time.h>

#define BUFLEN 128
#define QLEN 10

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif

#define QUEUESIZE 1
#define size_buff 1000

void ejm_c();

//MAIN
int main(int argc, char *argv[]){

    int option;
    char * serverIP, * serverPort, * remoteRoute, * localRoute;
    while((option= getopt (argc, argv, "i:p:R:L:")) != -1){
        switch(option){
            case 'i':
                serverIP = ( char * ) optarg;
                break;
            case 'p':
                serverPort = ( char * ) optarg;
                break;
            case 'R':
                remoteRoute = ( char * ) optarg;
                break;
            case 'L':
                localRoute = ( char * ) optarg;
                break;
            case '?':
                printf ( "opcion desconocida\n" );
                return 1;
            default:
                printf ( "opcion\n" );
                abort();
        }
    }

    //int sockdf; // NO SE PORQUE  CREO ESTA VARIABLE
    
// ./bin/cliente 127.0.0.1 45343 /home/herman/Downloads/eliminalo/taller5_file/estatico.txt ./temp/cp/local_sts.txt ----> para el taller
    if (argc!=9){ 
        printf("\nUSO: ./bin/cliente -i 127.0.0.1 -p 45343 -R /home/herman/Downloads/pic.png -L ./temp/downloaded/local.png\n");
        exit(-1);
    }

    long len_ruta= strlen(remoteRoute);

    printf("IP = %s\n",serverIP); //argv[1]);
    printf("PUERTO = %s\n" , serverPort); // argv[2]);
    printf("Ruta remota = %s\n", remoteRoute);//argv[3]);
    printf("Ruta local = %s\n", localRoute);//argv[4]);


    int puerto = atoi(serverPort);

    //Direccion del servidor, a la cual se le va a solicitar el archivo.
    struct sockaddr_in direccion_destino;

    //The C library function void *memset(void *str, int c, size_t n) copies the character c (an unsigned char) to the first n characters of the string pointed to, by the argument str.
    memset(&direccion_destino, 0 , sizeof(direccion_destino));

    //llenamos los campos
    direccion_destino.sin_family = AF_INET;
    direccion_destino.sin_port= htons(puerto);
    direccion_destino.sin_addr.s_addr=inet_addr(serverIP);

    int fd;
    //int err =0; // ! NO SE PORQUE LAS CREO

    // "direccion destino.sin_family" va en lugar de la direccion del archivo que el descriptor de archivos "open" hubiera direccionado para ser escrito o leido.
    if((fd = socket(direccion_destino.sin_family, SOCK_STREAM, 0))<0){
        perror ("Error al crear socket\n");
        return -1;
    }
    // en lugar de connect en la parte del servidor se tiene un bind
    int res = connect(fd,(struct sockaddr *)&direccion_destino, sizeof(direccion_destino));

    if(res<0){
        close(fd);
        perror ("Erorr al conectar\n");
        return -1;
    }

    // * cliente envia ruta del archivo al servidor por medio del socket "fd" (osea con analogia I/O,
    //   "fd" es el archivo previamente abierto abierto el cual va a escribir)
    // * Por eso el buffer del write es igual al length del la direccion del archivo "remoteRoute" 
    write (fd, remoteRoute, len_ruta+1);

    // ?printf("Cliente solicita archivo mediante write con 'env' ubicado en una ruta perteneciente al servidor con longitud: %d bytes\n", env-1);

    unsigned long bytescopiados = 0;
    char buf[size_buff] ={0};
    // int usado para el read que se incializara en las lines posteriores. Dentro del while.
    int rec=0;

    //Cliente espera respuesta, cuando hay informacion en el socket,
    //read le lee y retorna la cantindad de bytes que leyo. Si el otro
    // lado cierra el socket read retorna 0.

    // abro archivo donde se almacenara la informacion recibida
    // abro antes del while
    int d_file = open(localRoute, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    // manejo de erorres errores del open de d_file
    if (d_file < 0)
    {
        printf("errno= %d\n", errno);
        perror("Error al abrir el archivo\n");
        return -1;
    };
    // si quieres saca el printf de abajo no afecta en nada
    //printf("open_Dfile_ID=%d\n", d_file);

    // se crea un lazo while con read, para que cliente escuche todo lo que debe enviar el servidor hasta que ya no tenga que recibir nada
    while ((rec=read (fd, buf, 1000))>0){

        //char buf[size_buff] = {0};

        // declaro y creo el write, el cual almacenara los bytes ("buf") recibido (leido) por el descriptor de archivos "rec"
        // en el archivo abierto "d_file"
        unsigned long escrito = write(d_file, buf, rec);
        // manejo de erroes del write
        if (escrito <0 ){
            perror("escritos fue < 0");
            exit(-1);
        }
        // si no hay errores llevo control del numero de bytes escritos en el archivo "d_file"
        else{
            bytescopiados+=escrito;
        }
    }
    
    // cierro archivos abiertos
    close (fd);

    // el printf hace referencia a los bytes leidos, recibidos en el socket cliente por el servidor, y luego esos son los bytes que se los guarda localmente
    printf("bytes total descargados: %ld\n", bytescopiados);

    

}