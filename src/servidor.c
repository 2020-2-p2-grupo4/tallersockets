#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <time.h>

#define BUFLEN 128
#define QLEN 10

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif

#define QUEUESIZE 1
#define size_buff 1000

void ejm_c();

//Main
int main(int argc, char *argv[])
{

    int option;
    char * serverIP, * serverPort;
    while((option= getopt (argc, argv, "i:p:")) != -1){
        switch(option){
            case 'i':
                serverIP = ( char * ) optarg;
                break;
            case 'p':
                serverPort = ( char * ) optarg;
                break;
            case '?':
                printf ( "opcion desconocida\n" );
                return 1;
            default:
                printf ( "opcion\n" );
                abort();
        }
    }
    //./bin/servidor 127.0.0.1 45343 ----> para el taller

    if (argc != 5)
    {

        printf("\nUSO: ./bin/servidor -i 127.0.0.1 -p 45343\n"); // argv[0] argv[1] argv[2] ...
        exit(-1);
    }

    // imprimo los argumentos que recibe el programa servidor
    printf("Direccion: %s\nPuerto: %s\n", serverIP, serverPort);


    // El puerto del socket del servidor.
    int puerto = atoi(serverPort);

    // Direccion del servidor.
    struct sockaddr_in direccion_servidor;
    memset(&direccion_servidor, 0, sizeof(direccion_servidor));

    //llenamos los campos
    direccion_servidor.sin_family = AF_INET;
    direccion_servidor.sin_port = htons(puerto);
    direccion_servidor.sin_addr.s_addr = inet_addr(serverIP);

    int fd;
    //int err=0; // NO SE PORQUE LAS CREO

    if ((fd = socket(direccion_servidor.sin_family, SOCK_STREAM, 0)) < 0)
    {
        perror("Error al crear socket\n");
        return -1;
    }
    if (bind(fd, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor)) < 0)
    {
        perror("Error en bind \n");
        return -1;
    }
    if (listen(fd, QUEUESIZE) < 0)
    {
        perror("Erorr en liste\n");
        return -1;
    }
    
    // while del cual el servidor nunca sale, pues siempre esta oyendo si un cliente reliza alguna peticion.
    while (1)
    {
        // el accept como concepto es a la analogia con el "open" en I/O files,
        // "accept" abre un socket como el "open" abre eun archivo.
        // "accept" y "open" retornan un int, > 0 sie esta bién.
        // "accept" y "open" pueden ser utilizados por un "read", el caso del
        // taller para que este "read" lea lo que se encuentra en el
        // socket en el caso del "accept". Y de esta forma tambien lea el file
        // en el caso del "open"
        int sockfd_conectado = accept(fd, NULL, 0);
        //sleep(10000);
        //TODO...
        char buf[100] = {0};

        // server espera datos del cliente, lee desde el socket
        int leidos = read(sockfd_conectado, buf, 100);

        if (leidos < 0)
        {
            perror("Error al leer\n");
        }
        else
        {
            // recibimos por parte del cliente la ruta del archivo a enviar en la variable buf
            printf("Recibimos : %s\n", buf);

            umask(0);

            // abro la ruta "buf" del archivo que enviare
            int s_file = open(buf, O_RDONLY);

            // manejo de errores del open de s_file
            if (s_file < 0)
            {
                printf("errno= %d\n", errno);
                perror("Error al abrir el archivo\n");
            };
            printf("open_Sfile_ID=%d\n", s_file);

            // declaro el buffer donde guardo lo que leo del archivo
            char buf[size_buff] = {0};

            int env = 1;
            unsigned long bytescopiados = 0;

            // el proceso de copiado terminara cuando ya no hayan bytes que escribir.
            while (env != 0)
            {
                // inicializo el read con el buffer declarado
                // lo hago dentro del while para asegurarme que se lea el buffer tantas veces hasta el final del archivo
                unsigned long leido = read(s_file, buf, size_buff);
                // manejo de erroes del read
                if (leido < 0)
                {
                    perror("Leido fue < 0");
                    exit(-1);
                }
                // Se esccribe en el socket el archivo a enviar
                env = write(sockfd_conectado, buf, leido);
                if (env < 0)
                {
                    printf("error al enviar archivo\n");
                    return -1;
                }
                else
                {
                    bytescopiados += env;
                }
                //printf("Se enviarion %d bytes \n", env);
            }
            printf("bytes total escritos: %ld\n", bytescopiados);
            // cierro archivo que leo para luego enviar por el socket
            close(s_file);
        }
        // cierro el socket por donde recibo y envio datos
        close(sockfd_conectado);
    }
    
}//