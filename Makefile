all: bin/cliente bin/servidor

CC= gcc -Wall -fsanitize=address,undefined
obj1=cliente
obj11=F_client

obj2=servidor
obj22=F_server

bin/cliente: obj/$(obj1).o obj/$(obj11).o
	mkdir -p bin/
	mkdir -p temp/downloaded/
	$(CC) $^ -o $@

obj/$(obj1).o: src/$(obj1).c
	mkdir -p obj/
	$(CC)  -c $^ -o $@

obj/$(obj11).o: src/$(obj11).c
	$(CC) -c $^ -o $@
######################################################
bin/servidor: obj/$(obj22).o obj/$(obj2).o
	$(CC) $^ -o $@

obj/$(obj2).o: src/$(obj2).c
	$(CC)  -c $^ -o $@

obj/$(obj22).o: src/$(obj22).c
	$(CC)  -c $^ -o $@

.PHONY: clean
clean:
	rm -rf bin/
	rm -rf obj/
	rm -rf temp/downloaded/